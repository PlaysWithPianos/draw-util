.PHONY: all clean

.PRECIOUS: %/ %.d

%.d: ;

du_srcs := $(wildcard src/*.c)
du_objs := $(du_srcs:src/%.c=obj/%.o)
du_libs := -lm $(shell sdl2-config --libs) -lSDL2_ttf -lSDL2_image -lGL
du_cflags := -pthread $(shell sdl2-config --cflags)
du_cppflags :=
du_ldflags :=

all: libdrawutil.so libdrawutil.a

obj/:
	mkdir obj/

obj/%.o: src/%.c obj/%.d | obj/
	$(CC) -c -MMD -MP -fpic -g -Iinclude/ $(du_cppflags) $(CPPFLAGS) $(du_cflags) $(CFLAGS) $(TARGET_ARCH) $< -o $@

libdrawutil.so: $(du_objs)
	$(CC) -g -shared -fpic $(du_cflags) $(CFLAGS) $(TARGET_ARCH) $^ $(LDFLAGS) $(du_libs) $(LIBS) -o $@

libdrawutil.a: $(du_objs)
	$(AR) rcsv $@ $^

clean:
	$(RM) libdrawutil.so
	$(RM) $(du_objs)
	$(RM) $(du_objs:%.o=%.d)
