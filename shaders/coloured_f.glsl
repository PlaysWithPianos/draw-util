#version 330 core

out vec4 colour;
in vec4 frag_colour;
void main() {
	colour = frag_colour;
}

