#version 330 core

in vec3 vec_pos;
in vec4 vec_colour;
out vec4 frag_colour;
uniform mat4 transform;

void main() {
	vec4 vert_pos = vec4(vec_pos, 1.0);
	gl_Position = transform * vert_pos;
	frag_colour = vec_colour;
}
