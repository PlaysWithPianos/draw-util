#version 330 core

in vec3 vec_pos;
in vec2 vec_uv;

uniform mat4 transform;

out vec2 uv;

void main() {
	vec4 vert_pos = vec4(vec_pos, 1.0);
	gl_Position = transform * vert_pos;
	uv = vec_uv;
}
