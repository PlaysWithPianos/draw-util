#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "matrix.h"
#include "vector.h"

void matrix_debug(const struct matrix *m) {
	fprintf(stderr, "{\n");
	for(int i = 0; i < 4; i++) {
		for(int j = 0; j < 3; j++) {
			fprintf(stderr, "%f ", m->data[i][j]);
		}
		fprintf(stderr, "%f\n", m->data[i][3]);
	}
	fprintf(stderr, "}\n");
}

void matrix_multiply(struct matrix *dest, const struct matrix *a, const struct matrix *b) {
	// Because dest may be one of the inputs, we need to store the result temporarily and then copy it into dest when finished
	struct matrix m;
	for(int y = 0; y < 4; y++) {
		for(int x = 0; x < 4; x++) {
			m.data[y][x] = 0;
			for(int i = 0; i < 4; i++) {
				m.data[y][x] += a->data[y][i] * b->data[i][x];
			}
		}
	}
	memcpy(dest, &m, sizeof m);
}

const struct matrix matrix_IDENTITY = {
	{
		{1, 0, 0, 0},
		{0, 1, 0, 0},
		{0, 0, 1, 0},
		{0, 0, 0, 1}
	}
};

void matrix_identity(struct matrix *m) {
	*m = matrix_IDENTITY;
}

void matrix_perspective(struct matrix *m, float field_of_view, float aspect, float near, float far) {
	float y = near * tanf(field_of_view / 2);
	float x = y * aspect;
	matrix_frustum(m, -x, x, -y, y, near, far);
}

void matrix_frustum(struct matrix *m, float l, float r, float b, float t, float n, float f) {
	*m = (struct matrix) {
		{
			{2*n/(r-l), 0, (r+l)/(r-l), 0},
			{0, 2*n/(t-b), (t+b)/(t-b), 0},
			{0, 0, -(f+n)/(f-n), -2*f*n/(f-n)},
			{0, 0, -1, 0},
		}
	};
}

void matrix_translation(struct matrix *m, float x, float y, float z) {
	*m = (struct matrix) {
		{
			{1, 0, 0, 0},
			{0, 1, 0, 0},
			{0, 0, 1, 0},
			{x, y, z, 1},
		}
	};
}

void matrix_rotation(struct matrix *m, float x, float y, float z, float w) {
	float s = sinf(-w);
	float c = cosf(-w);
	float _c = 1-c;
	*m = (struct matrix) {
		{
			{x*x*_c+c, x*y*_c-z*s, x*z*_c+y*s, 0},
			{x*y*_c+z*s, y*y*_c+c, y*z*_c-x*s, 0},
			{x*z*_c-y*s, y*z*_c+x*s, z*z*_c+c, 0},
			{0, 0, 0, 1},
		}
	};
}

void matrix_scalar(struct matrix *m, float x, float y, float z) {
	*m = (struct matrix) {
		{
			{x, 0, 0, 0},
			{0, y, 0, 0},
			{0, 0, z, 0},
			{0, 0, 0, 1},
		}
	};
}

void matrix_vtranslation(struct matrix *m, const struct vector *v) {
	matrix_translation(m, v->x, v->y, v->z);
}

void matrix_vrotation(struct matrix *m, const struct vector *v, float w) {
	matrix_rotation(m, v->x, v->y, v->z, w);
}

void matrix_vscalar(struct matrix *m, const struct vector *v) {
	matrix_scalar(m, v->x, v->y, v->z);
}

void matrix_translate(struct matrix *m, float x, float y, float z) {
	struct matrix t;
	matrix_translation(&t, x, y, z);
	matrix_multiply(m, &t, m);
}

void matrix_vtranslate(struct matrix *m, const struct vector *v) {
	struct matrix t;
	matrix_vtranslation(&t, v);
	matrix_multiply(m, &t, m);
}

void matrix_rotate(struct matrix *m, float x, float y, float z, float w) {
	struct matrix t;
	matrix_rotation(&t, x, y, z, w);
	matrix_multiply(m, &t, m);
}

void matrix_vrotate(struct matrix *m, const struct vector *v, float w) {
	struct matrix t;
	matrix_vrotation(&t, v, w);
	matrix_multiply(m, &t, m);
}

void matrix_scale(struct matrix *m, float x, float y, float z) {
	struct matrix t;
	matrix_scalar(&t, x, y, z);
	matrix_multiply(m, &t, m);
}

void matrix_vscale(struct matrix *m, const struct vector *v) {
	struct matrix t;
	matrix_vscalar(&t, v);
	matrix_multiply(m, &t, m);
}
