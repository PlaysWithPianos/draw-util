#include <stdlib.h>
#include <errno.h>
#include <stdio.h>
#include <stdarg.h>
#include <limits.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_image.h>

#define GL_GLEXT_PROTOTYPES
#include <GL/gl.h>
#include <GL/glext.h>

#include "draw_util.h"
#include "matrix.h"

#define FONT_PATH "fonts/default"

static TTF_Font *font;

static GLuint default_vao;

struct shader {
	GLuint gl_program;
	GLint u_transform;
	GLint u_texture;
	GLint v_pos;
	GLint v_colour;
	GLint v_texpos;
};

static struct shader shader_coloured;
static struct shader shader_textured;

static SDL_Color active_colour;
static GLuint active_texture;

static struct matrix active_transform;

static int canvas_width;
static int canvas_height;

enum drawing_style {
	STYLE_COLOURED,
	STYLE_TEXTURED,
};

static enum drawing_style active_style = STYLE_COLOURED;

struct draw_target_texture {
	struct draw_target_texture *prev;
	GLuint frame_buffer;
	GLuint depth_buffer;
	GLuint target_texture;
	int width;
	int height;
};

static struct draw_target_texture *target_stack = NULL;

static unsigned init_flags;
static unsigned init_counter;

static void clear_gl_error(void) {
	while(glGetError() != GL_NO_ERROR);
}

static const char *get_gl_error_string(int error) {
	switch(error) {
		case GL_NO_ERROR:                      return "No error";
		case GL_INVALID_ENUM:                  return "Invalid enum";
		case GL_INVALID_VALUE:                 return "Invalid value";
		case GL_INVALID_OPERATION:             return "Invalid operation";
		case GL_INVALID_FRAMEBUFFER_OPERATION: return "Invalid framebuffer operation";
		case GL_OUT_OF_MEMORY:                 return "Out of memory";
		case GL_STACK_UNDERFLOW:               return "Stack underflow";
		case GL_STACK_OVERFLOW:                return "Stack overflow";
		default:                               return "Unknown error";
	}
}

void draw_dimensions(int *width, int *height) {
	if(target_stack) {
		*width = target_stack->width;
		*height = target_stack->height;
	} else {
		*width = canvas_width;
		*height = canvas_height;
	}
}

void draw_cleanup(void) {
	if(--init_counter) { return; }

	if(default_vao) {
		glDeleteVertexArrays(1, &default_vao);
	}

	default_vao = 0;

	if(shader_coloured.gl_program) {
		glDeleteProgram(shader_coloured.gl_program);
	}

	if(shader_textured.gl_program) {
		glDeleteProgram(shader_textured.gl_program);
	}

	shader_coloured = (struct shader){0};
	shader_textured = (struct shader){0};

	if(font) {
		TTF_CloseFont(font);
		font = NULL;
	}

	if((init_flags & DRAW_INIT_IMG_REQUIRED)) {
		IMG_Quit();
	}

	if((init_flags & DRAW_INIT_TTF_REQUIRED) && TTF_WasInit()) {
		TTF_Quit();
	}
}

#define BLOCK_SIZE 0x1000

static int load_file(const char *path, char **text, int *length) {
	FILE *f = fopen(path, "rb");
	if(!f) { return 0; }
	int len = 0;
	int max = 0;
	int received;
	char buf[BLOCK_SIZE];
	char *data = NULL;

	do {
		received = fread(buf, 1, BLOCK_SIZE, f);
		if(!received) { break; }
		if((len + received) > max) {
			if(max >= INT_MAX / 2) {
				free(data);
				fclose(f);
				errno = ENOMEM;
				return 0;
			}
			int newmax = (max ? max * 2 : BLOCK_SIZE);
			char *newdata = realloc(data, newmax);
			if(!newdata) {
				free(data);
				fclose(f);
				return 0;
			}
			data = newdata;
			max = newmax;
		}
		memcpy(&data[len], buf, received);
		len += received;
	} while(received);

	*text = data;
	*length = len;
	return 1;
}

#undef BLOCK_SIZE

static GLuint load_shader(GLenum shader_type, const char *path) {
	char *data;
	int len;
	if(!load_file(path, &data, &len)) { fprintf(stderr, "Failed to load shader from file %s\n", path); return 0; }
	GLuint shader = glCreateShader(shader_type);
	if(!shader) { free(data); fprintf(stderr, "Shader creation failed\n"); return 0; }

	glShaderSource(shader, 1, (const char **)&data, &len);
	free(data);

	glCompileShader(shader);

	glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &len);
	if(len > 0) {
		data = malloc(len);
		if(data) {
			glGetShaderInfoLog(shader, len, NULL, data);
			fprintf(stderr, "%s: %s", path, data);
			free(data);
		} else { glDeleteShader(shader); return 0; }
	}

	int ok;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &ok);
	if(!ok) { glDeleteShader(shader); fprintf(stderr, "Shader compilation failed\n"); return 0; }

	return shader;
}

static _Bool load_shader_program(struct shader *shader, const char *vert, const char *frag) {
	GLuint program = glCreateProgram();
	if(!program) { fprintf(stderr, "Shader Program creation failed\n"); return 0; }
	GLuint vshader = load_shader(GL_VERTEX_SHADER, vert);
	GLuint fshader = load_shader(GL_FRAGMENT_SHADER, frag);

	glAttachShader(program, vshader);
	glAttachShader(program, fshader);
	glLinkProgram(program);

	glDeleteShader(vshader);
	glDeleteShader(fshader);

	int len;
	char *info_log;

	glGetProgramiv(program, GL_INFO_LOG_LENGTH, &len);
	if(len > 0) {
		info_log = malloc(len);
		if(info_log) {
			glGetProgramInfoLog(program, len, NULL, info_log);
			fprintf(stderr, "(%s, %s): %s", vert, frag, info_log);
			free(info_log);
		} else { glDeleteProgram(program); return 0; }
	}

	int ok;
	glGetProgramiv(program, GL_LINK_STATUS, &ok);
	if(!ok) { glDeleteProgram(program); fprintf(stderr, "Shader program linking failed\n"); return 0; }

	*shader = (struct shader) {
		.gl_program = program,
		.u_transform = glGetUniformLocation(program, "transform"),
		.u_texture = glGetUniformLocation(program, "tex"),
		.v_pos = glGetAttribLocation(program, "vec_pos"),
		.v_colour = glGetAttribLocation(program, "vec_colour"),
		.v_texpos = glGetAttribLocation(program, "vec_uv"),
	};

	return 1;
}

static _Bool load_shaders(void) {
	return
		load_shader_program(&shader_coloured, "shaders/coloured_v.glsl", "shaders/coloured_f.glsl") &&
		load_shader_program(&shader_textured, "shaders/textured_v.glsl", "shaders/textured_f.glsl");
}

int draw_init(unsigned flags) {
	init_counter++;

	unsigned old_flags = init_flags;
	init_flags |= flags;
	if(flags & DRAW_INIT_TTF_REQUIRED & ~old_flags) {
		if(TTF_Init() < 0) {
			draw_cleanup();
			fprintf(stderr, "TTF_Init failed\n");
			return 0;
		}
	}

	if(flags & DRAW_INIT_IMG_REQUIRED & ~old_flags) {
		if(IMG_Init(IMG_INIT_PNG | IMG_INIT_JPG) < 0) {
			draw_cleanup();
			fprintf(stderr, "IMG_Init failed\n");
			return 0;
		}
	}

	if(init_counter > 1) { return 1; }

	font = TTF_OpenFont(FONT_PATH, 20);
	if(!font) {
		draw_cleanup();
		fprintf(stderr, "Failed to open font %s\n", FONT_PATH);
		return 0;
	}

	glGenVertexArrays(1, &default_vao);
	if(!default_vao) {
		draw_cleanup();
		return 0;
	}
	glBindVertexArray(default_vao);

	if(!load_shaders()) {
		draw_cleanup();
		return 0;
	}

	SDL_GL_SetSwapInterval(1);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	matrix_identity(&active_transform);
	return 1;
}

void draw_image(int x, int y, int w, int h, GLuint texture) {
	glBindVertexArray(default_vao);

	GLuint buf;
	glGenBuffers(1, &buf);
	glBindBuffer(GL_ARRAY_BUFFER, buf);

	glUseProgram(shader_textured.gl_program);

	glBindTexture(GL_TEXTURE_2D, texture);

	int cw, ch;
	draw_dimensions(&cw, &ch);

	struct matrix m;
	matrix_identity(&m);
	matrix_translate(&m, -1, -1, 0);
	matrix_scale(&m, 2.0/cw, 2.0/ch, 1);

	glEnableVertexAttribArray(shader_textured.v_pos);
	glEnableVertexAttribArray(shader_textured.v_texpos);

	glUniformMatrix4fv(shader_textured.u_transform, 1, 0, (float *)m.data);

	int tl = 0;
	int tb = 0;
	int tr = 1;
	int tt = 1;

	if(w < 0) {
		tl = 1;
		tr = 0;
		w = -w;
	}

	if(h < 0) {
		tb = 1;
		tt = 0;
		h = -h;
	}

	int l = x;
	int b = y;
	int r = x + w;
	int t = y + h;

	float data[] = {
		l, b, 0, tl, tb,
		r, b, 0, tr, tb,
		r, t, 0, tr, tt,
		r, t, 0, tr, tt,
		l, t, 0, tl, tt,
		l, b, 0, tl, tb,
	};

	glBufferData(GL_ARRAY_BUFFER, sizeof data, data, GL_STREAM_DRAW);
	glVertexAttribPointer(shader_textured.v_pos, 3, GL_FLOAT, GL_TRUE, 5 * sizeof(float), (const GLvoid *)0);
	glVertexAttribPointer(shader_textured.v_texpos, 2, GL_FLOAT, GL_TRUE, 5 * sizeof(float), (const GLvoid *)(3 * sizeof(float)));

	glDrawArrays(GL_TRIANGLES, 0, 6);
	glDeleteBuffers(1, &buf);
}

static GLuint render_text(const char *text, int *pw, int *ph) {
	SDL_Surface *surface = TTF_RenderUTF8_Blended(font, text, active_colour);
	if(!surface) { return 0; }

	int w = surface->w;
	int h = surface->h;

	if(pw) { *pw = w; }
	if(ph) { *ph = h; }

	SDL_Surface *converted = SDL_ConvertSurfaceFormat(surface, SDL_PIXELFORMAT_ABGR8888, 0);
	SDL_FreeSurface(surface);
	surface = converted;
	if(!surface) { return 0; }

	GLuint texture;
	glCreateTextures(GL_TEXTURE_2D, 1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, surface->pixels);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	SDL_FreeSurface(surface);

	return texture;
}

void draw_text_at_size(const char *text, int x, int y, int *w, int *h) {
	if(!(text && text[0])) { return; }
	int _w, _h;
	GLuint texture = render_text(text, &_w, &_h);
	draw_image(x, y, _w, -_h, texture);
	glDeleteTextures(1, &texture);
	if(w) { *w = _w; }
	if(h) { *h = _h; }
}

void draw_text_at(const char *text, int x, int y) {
	draw_text_at_size(text, x, y, NULL, NULL);
}

void draw_text(const char *text) {
	if(!(text && text[0])) { return; }
	int w, h;
	GLuint texture = render_text(text, NULL, NULL);
	draw_dimensions(&w, &h);
	draw_image(0, 0, w, -h, texture);
	glDeleteTextures(1, &texture);
}

void draw_text_size(const char *text, int *w, int *h) {
	if(!text) {
		if(w) { *w = 0; }
		if(h) { *h = 0; }
	} else {
		TTF_SizeUTF8(font, text, w, h);
	}
}

struct draw_gl_attribute {
	SDL_GLattr attr;
	const char *str;
	int value;
};

#define X(A) A, #A
static struct draw_gl_attribute desired_gl_attributes[] = {
	{ X(SDL_GL_CONTEXT_MAJOR_VERSION), 3 },
	{ X(SDL_GL_CONTEXT_MINOR_VERSION), 3 },
	{ X(SDL_GL_DOUBLEBUFFER), 1 },
	{ X(SDL_GL_ACCELERATED_VISUAL), 1},
};

#define draw_num_gl_attributes (sizeof desired_gl_attributes / sizeof *desired_gl_attributes)

void draw_set_opengl_attributes(void) {
	for(int i = 0; i < draw_num_gl_attributes; i++) {
		struct draw_gl_attribute *attr = &desired_gl_attributes[i];
		SDL_GL_SetAttribute(attr->attr, attr->value);
	}
}

_Bool draw_check_opengl_attributes(void) {
	for(int i = 0; i < draw_num_gl_attributes; i++) {
		struct draw_gl_attribute *attr = &desired_gl_attributes[i];
		int value;
		SDL_GL_GetAttribute(attr->attr, &value);
		if(value != attr->value) {
			return 0;
		}
	}
	return 1;
}

void draw_set_colour(unsigned char red, unsigned char green, unsigned char blue) {
	active_colour = (SDL_Color) { red, green, blue, 0xff };
	active_style = STYLE_COLOURED;
}

void draw_set_texture(GLuint texture) {
	active_texture = texture;
	active_style = STYLE_TEXTURED;
}

void draw_resized(int x, int y, int w, int h) {
	canvas_width = w;
	canvas_height = h;
	glViewport(x, y, w, h);
}

_Bool draw_texture(int width, int height) {
	struct draw_target_texture *target = malloc(sizeof *target);
	if(!target) {
		return 0;
	}

	glBindVertexArray(default_vao);

	clear_gl_error();

	GLuint framebuffer;
	glGenFramebuffers(1, &framebuffer);
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, framebuffer);

	GLuint depth_buffer;
	glGenRenderbuffers(1, &depth_buffer);
	glBindRenderbuffer(GL_RENDERBUFFER, depth_buffer);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, width, height);
	glFramebufferRenderbuffer(GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depth_buffer);

	GLuint texture;
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture, 0);

	glDrawBuffers(1, (GLenum[]) { GL_COLOR_ATTACHMENT0 });
	glViewport(0, 0, width, height);

	target->prev = target_stack;
	target->frame_buffer = framebuffer;
	target->depth_buffer = depth_buffer;
	target->target_texture = texture;
	target->width = width;
	target->height = height;

	target_stack = target;

	int error = glGetError();
	if(error != GL_NO_ERROR) {
		fprintf(stderr, "Error: %s", get_gl_error_string(error));
		int texture = draw_texture_end();
		glDeleteTextures(1, &texture);
		return 0;
	}

	return 1;
}

GLuint draw_texture_end(void) {
	if(!target_stack) { return 0; }

	struct draw_target_texture *old_target = target_stack;
	struct draw_target_texture *new_target = old_target->prev;
	target_stack = new_target;

	GLuint texture = old_target->target_texture;

	if(new_target) {
		glBindFramebuffer(GL_DRAW_FRAMEBUFFER, new_target->frame_buffer);
		glViewport(0, 0, new_target->width, new_target->height);
	} else {
		glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
		glViewport(0, 0, canvas_width, canvas_height);
	}

	glDeleteFramebuffers(1, &old_target->frame_buffer);
	glDeleteRenderbuffers(1, &old_target->depth_buffer);

	free(old_target);

	return texture;
}

GLuint draw_load_texture(const char *image, unsigned flags, int *w, int *h) {
	SDL_Surface *surface = IMG_Load(image);
	if(!surface) { fprintf(stderr, "Failed to load texture: %s\n", image); return 0; }

	SDL_Surface *converted = SDL_ConvertSurfaceFormat(surface, SDL_PIXELFORMAT_ABGR8888, 0);
	SDL_FreeSurface(surface);
	surface = converted;
	if(!surface) { return 0; }

	GLuint texture;
	glCreateTextures(GL_TEXTURE_2D, 1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, surface->w, surface->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, surface->pixels);

	if(w) { *w = surface->w; }
	if(h) { *h = surface->h; }

	if(flags & DRAW_TEXTURE_NEAREST) {
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	} else {
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	}

	if(flags & DRAW_TEXTURE_NO_WRAP) {
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	}

	SDL_FreeSurface(surface);

	return texture;
}

GLuint draw_prerender_text(const char *text, int *w, int *h) {
	int width, height;
	glBindVertexArray(default_vao);
	draw_text_size(text, &width, &height);
	if(!draw_texture(width, height)) { return 0; }
	glClearColor(0,0,0,0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	draw_text(text);
	if(w) { *w = width; }
	if(h) { *h = height; }
	return draw_texture_end();
}

struct matrix *draw_transform(struct matrix *copy) {
	if(copy) { *copy = active_transform; }
	return &active_transform;
}

void draw_delete_shape(struct drawing_shape *shape) {
	glDeleteVertexArrays(1, &shape->vao);
	glDeleteBuffers(1, &shape->buffer);
	if(shape->textured) {
		glDeleteTextures(1, &shape->texture);
	}
}

struct drawing_shape draw_prepare_shape(GLenum mode, int num_points, float *data) {
	struct drawing_shape shape = {
		.verts = num_points,
		.glmode = mode,
	};

	glGenVertexArrays(1, &shape.vao);
	glBindVertexArray(shape.vao);

	glGenBuffers(1, &shape.buffer);
	glBindBuffer(GL_ARRAY_BUFFER, shape.buffer);

	switch(active_style) {
		case STYLE_COLOURED:
			glEnableVertexAttribArray(shader_coloured.v_pos);
			glEnableVertexAttribArray(shader_coloured.v_colour);
			shape.program = shader_coloured.gl_program;
			shape.transform = shader_coloured.u_transform;
			glBufferData(GL_ARRAY_BUFFER, shape.verts * 7 * sizeof(float), data, GL_STATIC_DRAW);
			glVertexAttribPointer(shader_coloured.v_pos, 3, GL_FLOAT, GL_TRUE, 7 * sizeof(float), (const GLvoid *)0);
			glVertexAttribPointer(shader_coloured.v_colour, 4, GL_FLOAT, GL_TRUE, 7 * sizeof(float), (const GLvoid *)(3 * sizeof(float)));
			break;
		case STYLE_TEXTURED:
			shape.textured = 1;
			shape.texture = active_texture;
			glEnableVertexAttribArray(shader_textured.v_pos);
			glEnableVertexAttribArray(shader_textured.v_texpos);
			shape.program = shader_textured.gl_program;
			shape.transform = shader_textured.u_transform;
			glBufferData(GL_ARRAY_BUFFER, shape.verts * 5 * sizeof(float), data, GL_STATIC_DRAW);
			glVertexAttribPointer(shader_textured.v_pos, 3, GL_FLOAT, GL_TRUE, 5 * sizeof(float), (const GLvoid *)0);
			glVertexAttribPointer(shader_textured.v_texpos, 2, GL_FLOAT, GL_TRUE, 5 * sizeof(float), (const GLvoid *)(3 * sizeof(float)));
			break;
	}

	return shape;
}

void draw_shape(const struct drawing_shape *shape) {
	glBindVertexArray(shape->vao);
	glUseProgram(shape->program);
	glUniformMatrix4fv(shape->transform, 1, 0, (float *)&active_transform.data);
	if(shape->textured) {
		glBindTexture(GL_TEXTURE_2D, shape->texture);
	}
	glDrawArrays(shape->glmode, 0, shape->verts);
}

struct drawing_shape draw_prepare_rectangle(float left, float bottom, float width, float height) {
	float texleft = 0;
	float texright = 1;
	float textop = 0;
	float texbottom = 1;
	if(width < 0) {
		texleft = 1;
		texright = 0;
		width = -width;
	}
	if(height < 0) {
		textop = 1;
		texbottom = 0;
		height = -height;
	}
	float *data;
	float right = left + width;
	float top = bottom + height;
	float red = active_colour.r / 255.0;
	float green = active_colour.g / 255.0;
	float blue = active_colour.b / 255.0;
	float alpha = active_colour.a / 255.0;
	switch(active_style) {
		case STYLE_TEXTURED:
			data = (float[]) {
				left, bottom, 0, texleft, texbottom,
				right, bottom, 0, texright, texbottom,
				right, top, 0, texright, textop,
				right, top, 0, texright, textop,
				left, top, 0, texleft, textop,
				left, bottom, 0, texleft, texbottom,
			};
			break;
		case STYLE_COLOURED:
			data = (float[]) {
				left, bottom, 0, red, green, blue, alpha,
				right, bottom, 0, red, green, blue, alpha,
				right, top, 0, red, green, blue, alpha,
				right, top, 0, red, green, blue, alpha,
				left, top, 0, red, green, blue, alpha,
				left, bottom, 0, red, green, blue, alpha,
			};
			break;
	}
	return draw_prepare_shape(GL_TRIANGLES, 6, data);
}

struct drawing_shape draw_prepare_object(int num_triangles, float *data) {
	return draw_prepare_shape(GL_TRIANGLES, num_triangles * 3, data);
}

struct drawing_shape draw_prepare_triangle(float *data) {
	return draw_prepare_object(1, data);
}
