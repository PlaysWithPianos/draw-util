#include <math.h>
#include <GL/gl.h>

#include "camera.h"
#include "matrix.h"

// Number of steps in a circle
#define ANGLE_STEPS 256

// Number of zoom levels to double zoom factor
#define ZOOM_STEPS 16

static float angle(float a) {
	return a * M_PI / (ANGLE_STEPS / 2.0f);
}

void camera_view(struct camera *cam, struct matrix *m) {
	float zoom_factor = powf(2, cam->zoom/ZOOM_STEPS);

	matrix_identity(m);

	matrix_translate(m, 0, -cam->z, 0);
	matrix_rotate(m, 1, 0, 0, angle(cam->xr));
	matrix_rotate(m, 0, 1, 0, angle(cam->yr));
	matrix_translate(m, cam->x, 0, -cam->y);
	matrix_scale(m, zoom_factor, zoom_factor, zoom_factor);
}

void camera_move(struct camera *cam, float right, float forward, float up) {
	float s = sinf(angle(cam->yr))/2;
	float c = cosf(angle(cam->yr))/2;

	cam->x += s * forward - c * right;
	cam->y += c * forward + s * right;

	cam->z += up;
}

static void rotate(float *angle, float amount) {
	*angle += amount;
	if(*angle > ANGLE_STEPS / 2) { *angle -= ANGLE_STEPS; }
	if(*angle <= -ANGLE_STEPS / 2) { *angle += ANGLE_STEPS; }
}

void camera_turn(struct camera *c, float a) {
	rotate(&c->yr, a);
}

void camera_change_zoom(struct camera *c, float z) {
	c->zoom += z;
}

void camera_tilt(struct camera *c, float x) {
	rotate(&c->xr, x);
}

void camera_activate(struct camera *c) {
	glDepthRange(c->depth_min, c->depth_max);
}
