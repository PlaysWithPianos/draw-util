#ifndef VECTOR_H
#define VECTOR_H 1

struct vector {
	float x;
	float y;
	float z;
};

struct quaternion {
	float w;
	struct vector v;
};

#endif /* VECTOR_H */
