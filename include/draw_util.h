#ifndef DRAW_UTIL_H
#define DRAW_UTIL_H 1

#include <GL/gl.h>
#include "matrix.h"

// Texture Flags

// Indicates that sampling this texture should be done without interpolation.
#define DRAW_TEXTURE_NEAREST 1
// Indicates that the edges of this texture do not join.
#define DRAW_TEXTURE_NO_WRAP 2

// Drawing functions will usually use the currently active texture or colour.
// They will also usually use the active transformation matrix (retreived by draw_transform()).
// There are no protections for multi-threaded use / access in any of these functions.
// Textures returned by texture-generating functions are simply openGL texture identifiers, so they can be used as arguments for relevant openGL functions.
// In particular, you can delete a texture with glDeleteTextures(1, &texture)

// Called to initialise drawing system.
// This must be called before any other drawing function (except draw_{set/check}_opengl_attributes), but only after an openGL context has been created and activated.

#define DRAW_INIT_IMG_REQUIRED 1
#define DRAW_INIT_TTF_REQUIRED 2

int draw_init(unsigned flags);

// Call to clean-up all the mess left behind by draw_init.
// Drawing system is uninitialised after this, but may be re-initialised by calling draw_init again.
void draw_cleanup(void);

// These are the only functions (in this file) that can be called before draw_init.
// draw_set_opengl_attributes lets SDL know about some capabilites we want our openGL context to have.
// It must be called BEFORE CREATING ANY WINDOWS.
void draw_set_opengl_attributes(void);
// draw_check_opengl_attributes should be called after creating an opengl context.
// It will verify that the context supports our required capabilites.
_Bool draw_check_opengl_attributes(void);

// Return the size of the drawing canvas (in pixels).
void draw_dimensions(int *x, int *y);

// Change the size of the drawing canvas, and adjust the viewport.
void draw_resized(int x, int y, int w, int h);

// Draw some text with the default font and active colour.
void draw_text(const char *text);
// Draw some text with a given offset.
void draw_text_at(const char *text, int x, int y);
// Measure the rectangle that some text would occupy, but do not draw it.
void draw_text_size(const char *text, int *w, int *h);
// Measure AND draw some text.
void draw_text_at_size(const char *text, int x, int y, int *w, int *h);

// Set the active colour
// Shapes & text will use this colour
void draw_set_colour(unsigned char red, unsigned char green, unsigned char blue);

// Set the active texture
// Shapes will use this texture, text will continue to use the active colour
void draw_set_texture(GLuint texture);

// Draw an image directly to the canvas, ignoring the transformation matrix.
// Measures from bottom left of canvas, with (x,y) being the bottom left corner of the image.
// Specifying a negative value for w or h will flip the image horizontally or vertically.
void draw_image(int x, int y, int w, int h, GLuint texture);

// Create a new canvas that will be made into a texture.
// This allows for offscreen rendering.
// Call draw_texture_end to finalise the new texture.
// There is an internal stack of canvases, so you can call draw_texture again before draw_texture_end.
// There must be one call to draw_texture_end for each call to draw_texture.
_Bool draw_texture(int width, int height);
GLuint draw_texture_end(void);

// Load a texture from a file.
// .png, .jpg, .bmp, and some other formats are supported (see SDL2_image docs for full list).
// Regardless of format, images are converted to 32-bit RGBA for use with openGL.
GLuint draw_load_texture(const char *image, unsigned flags, int *w, int *h);

// Make a texture out of some text.
// This is the preferred way to draw text, particularly if it will never change, as draw_text will create a new texture every time you call it, which can be slow.
GLuint draw_prerender_text(const char *text, int *width, int *height);

// Retreive a pointer to the transformation matrix.
// This matrix is passed to the vertex shader when drawing shapes to convert their position in 3d space to 2d screen space.
// If copy is non-null, then the current matrix will be stored there (useful if you want to restore the transformation matrix later).
struct matrix *draw_transform(struct matrix *copy);

// Drawing shapes represent objects that are ready to be drawn.
// You may modify this, but be careful.
// In particular, remember that coloured shapes store 7 floats per vertex (x,y,z,r,g,b,a), but textured shapes only 5 (x,y,z,u,v)
// So it probably isn't a good idea to switch between textured/coloured.
// The transform field should only be changed if you are swapping out the shader.
// You can safely replace the texture.
struct drawing_shape {
	GLuint vao; // Vertex array object
	GLuint buffer; // Vertex buffer object
	GLuint program; // Shader Program
	int transform; // Location of shader's transform uniform
	int verts; // Number of vertices
	GLenum glmode; // Drawing mode (most likely GL_TRIANGLES)
	GLuint texture; // texture id (if textured)
	_Bool textured; // If set, use the stored texture, otherwise use colours.
};

// Delete the vertex buffer and vertex array belonging to this shape.
// If it is textured, also delete the texture.
// If you wish to re-use the texture, simply set shape->textured = false before deleting it.
void draw_delete_shape(struct drawing_shape *shape);

// Draw a shape
void draw_shape(const struct drawing_shape *shape);

// Prepare shapes for drawing

// Prepare an abitrary shape.
// With an active texture, makes a textured shape, whith 5 values per vertex (x,y,z,u,v)
// With an active colour, makes a coloured shape, with 7 values per vertex (x,y,z,r,g,b,a)
// The actual active colour is ignored.
struct drawing_shape draw_prepare_shape(GLenum mode, int num_points, float *points);
// Prepare an 'object' made of triangles. All other considerations of draw_prepare_shape apply.
struct drawing_shape draw_prepare_object(int num_triangles, float *points);
// Prepare a single triangle. All other considerations of draw_prepare_shape_apply.
struct drawing_shape draw_prepare_triangle(float *points);
// Prepare a rectangle.
// The rectangle will be drawn with its lower-left corner at (left,bottom) (and then modified by the transformation matrix).
// If a colour is active, the entire rectangle will be that colour.
// If a texture is active, the rectangle will be textured with it.
struct drawing_shape draw_prepare_rectangle(float left, float bottom, float width, float height);

#endif /* DRAW_UTIL_H */
