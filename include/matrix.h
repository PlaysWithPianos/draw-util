#ifndef ENGINE_MATRIX_H
#define ENGINE_MATRIX_H 1

struct matrix {
	float data[4][4];
};

extern const struct matrix matrix_IDENTITY;

void matrix_identity(struct matrix *m);
void matrix_frustum(struct matrix *m, float l, float r, float b, float t, float n, float f);
void matrix_perspective(struct matrix *m, float field_of_view, float aspect, float near, float far);
void matrix_multiply(struct matrix *dest, const struct matrix *a, const struct matrix *b);

struct vector;

void matrix_translation(struct matrix *m, float x, float y, float z);
void matrix_vtranslation(struct matrix *m, const struct vector *v);
void matrix_rotation(struct matrix *m, float x, float y, float z, float w);
void matrix_vrotation(struct matrix *m, const struct vector *v, float w);
void matrix_scalar(struct matrix *m, float x, float y, float z);
void matrix_vscalar(struct matrix *m, const struct vector *v);

void matrix_translate(struct matrix *m, float x, float y, float z);
void matrix_vtranslate(struct matrix *m, const struct vector *v);
void matrix_rotate(struct matrix *m, float x, float y, float z, float w);
void matrix_vrotate(struct matrix *m, const struct vector *v, float w);
void matrix_scale(struct matrix *m, float x, float y, float z);
void matrix_vscale(struct matrix *m, const struct vector *v);

void matrix_debug(const struct matrix *m);

#endif /* MATRIX_H */
