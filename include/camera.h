#ifndef CAMERA_H
#define CAMERA_H 1

#include "matrix.h"

// Simulate a camera or viewpoint in 3d space.

struct camera {
	// Position, angle, and zoom level of camera.
	float x, y, z, xr, yr, zoom;
	// Projection matrix converts screen space into world space.
	// This is not used or modified by any functions here, but is associated with a camera, so is stored with it.
	struct matrix projection;
	// These are passed to glDepthRange when calling camera_activate
	float depth_min, depth_max;
};

// Calculate view matrix for camera.
// View matrix converts world space into camera space.
void camera_view(struct camera *cam, struct matrix *m);

// Move (translate) a camera (based on where the camera is pointing).
void camera_move(struct camera *cam, float right, float forward, float up);

// Rotate left/right
void camera_turn(struct camera *c, float a);
// Rotate up/down
void camera_tilt(struct camera *c, float x);
// Zoom in/out
void camera_change_zoom(struct camera *c, float z);
// Make a camera active
void camera_activate(struct camera *c);

#endif /* CAMERA_H */
